package com.devcamp.s55.restapi;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class CDailyCampaign {
    @CrossOrigin
	@GetMapping("/devcamp-date")
	public String getDateViet() {
		DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        switch(dtfVietnam.format(today)){
            case "Thứ Hai" :
            return String.format(" Thứ Hai Hello pizza lover ! hôm nay %s , mua 1 tặng 1.", dtfVietnam.format(today));
            case "Thứ Ba" :
            return String.format(" Thứ Ba tặng tất cả khách hàng một phần bánh ngọt ", dtfVietnam.format(today));
            default :
            return String.format("Hello pizza lover ! hôm nay %s , mua 1 tặng 1.", dtfVietnam.format(today));
        }      
	}
}
