var gSelectedMenuStructure = {
    menuName: "", // S, M, L
    duongKinhCM: 0,
    suonNuong: 0,
    saladGr: 0,
    drink: 0,
    priceVND: 0
}

var gSelectedPizzaType = "";
var gDrinkList = [];
var gCustomerInfo = {
    menuCombo: null,
    loaiPizza: "",
    ten: "",
    email: "",
    dienThoai: "",
    diaChi: "",
    loiNhan: "",
    nuocUong: "",
    voucher: ""
};
var gVoucherObj = [];
var gPercent = 0;
$(document).ready(function() {
    onPageLoading();
    $("#btn-s-small").on("click", function() {
        console.log("Combo Small được chọn!");
        onBtnChangeColor("Small");
        gSelectedMenuStructure = getComboSelected("S", 20, 2, 200, 2, 150000);
        console.log(gSelectedMenuStructure);
    })
    $("#btn-m-medium").on("click", function() {
        console.log("Combo Medium được chọn!");
        onBtnChangeColor("Medium");
        gSelectedMenuStructure = getComboSelected("M", 25, 4, 300, 3, 200000);
        console.log(gSelectedMenuStructure);
    })
    $("#btn-l-large").on("click", function() {
        console.log("Combo Large được chọn!");
        onBtnChangeColor("Large");
        gSelectedMenuStructure = getComboSelected("L", 30, 8, 500, 4, 250000);
        console.log(gSelectedMenuStructure);
    })
    $("#btn-ocean").on("click", function() {
        onBtnChangeColor("Ocean Mania");
        gSelectedPizzaType = "Ocean Mania";
        console.log("Loại Pizza " + gSelectedPizzaType + " được chọn!");
    })
    $("#btn-hawaii").on("click", function() {
        console.log("Loại Pizza Hawaiian được chọn!");
        onBtnChangeColor("Hawaiian");
        gSelectedPizzaType = "Hawaiian";
        console.log("Loại Pizza " + gSelectedPizzaType + " được chọn!");
    })
    $("#btn-bacon").on("click", function() {
        onBtnChangeColor("Cheesy Chicken Bacon");
        gSelectedPizzaType = "Cheesy Chicken Bacon";
        console.log("Loại Pizza " + gSelectedPizzaType + " được chọn!");
    })
    $("#send-data").on("click", function() {
        gCustomerInfo = getCustomerInfo();
        var vCheck = validateData(gCustomerInfo);
        if (vCheck == true) {
            console.log("Dữ liệu hợp lệ!");
            console.log(gCustomerInfo);
            $("#order-detail-modal").modal("show");
            getCustomerInfo();
            callApiToGetVoucher(gCustomerInfo.voucher);
        }
        $("#btn-ok").on("click", function() {
            location.reload();
            $(window).scrollTop(0);
        })
        $("#btn-create-order").on("click", function() {
            var vOrderId = "";
            var vObjectRequest = {
                kichCo: gCustomerInfo.menuCombo.menuName,
                duongKinh: gCustomerInfo.menuCombo.duongKinhCM,
                suon: gCustomerInfo.menuCombo.suonNuong,
                salad: gCustomerInfo.menuCombo.saladGr,
                loaiPizza: gCustomerInfo.loaiPizza,
                idVourcher: gCustomerInfo.voucher,
                idLoaiNuocUong: gCustomerInfo.nuocUong,
                soLuongNuoc: gCustomerInfo.menuCombo.drink,
                hoTen: gCustomerInfo.ten,
                thanhTien: gCustomerInfo.menuCombo.priceVND,
                email: gCustomerInfo.email,
                soDienThoai: gCustomerInfo.dienThoai,
                diaChi: gCustomerInfo.diaChi,
                loiNhan: gCustomerInfo.loiNhan
            }
            $.ajax({
                url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
                async: false,
                type: "POST",
                data: JSON.stringify(vObjectRequest),
                contentType: "application/json;charset=UTF-8",
                success: function(responseObject) {
                    console.log(responseObject);
                    vOrderId = responseObject.orderId;
                },
                error: function(error) {
                    alert(error.responseText);
                }
            })
            $("#order-detail-modal").modal("hide");
            $("#order-id-modal").modal("show");
            $("#inp-modal-order-id").val(vOrderId);
        })
    })

    function getApiNgayThang() {
        const BASE_URL = "http://localhost:8080/campaigns/devcamp-date";
        // create a request
        $.ajax({
            async: false,
            url: BASE_URL,
            type: "GET",
            contentType: "application/json;charset=UTF-8",
            success: function(res) {
                var quoteList = res;
                console.log(quoteList);
                $("#marketing").html(quoteList);
            },
            error: function(error) {
                console.assert(error)
            }
        });
    }
    /* function callApiToCreateOrder(){
         

       var vXmlHttpCreateOrder = new XMLHttpRequest();
         vXmlHttpCreateOrder.open("POST", vBASE_URL, true);
         vXmlHttpCreateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
         vXmlHttpCreateOrder.send(JSON.stringify(vObjectRequest));
         vXmlHttpCreateOrder.onreadystatechange =
             function () {
                 if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_CREATE_OK) {
                     var vCreatedOrder = vXmlHttpCreateOrder.responseText;
                     console.log(vCreatedOrder);
                 }
             }
     } */
    function onBtnChangeColor(paramButton) {
        "use strict";
        var vSmallSize = $("#btn-s-small");
        var vMediumSize = $("#btn-m-medium");
        var vLargeSize = $("#btn-l-large");
        var vOcean = $("#btn-ocean");
        var vHawaiian = $("#btn-hawaii");
        var vBacon = $("#btn-bacon");
        if (paramButton === "Small") {
            vSmallSize.addClass("btn-warning");
            vMediumSize.removeClass("btn-warning");
            vMediumSize.addClass("btn-success");
            vLargeSize.removeClass("btn-warning");
            vLargeSize.addClass("btn-success");
        }
        if (paramButton === "Medium") {
            vMediumSize.addClass("btn-warning");
            vSmallSize.removeClass("btn-warning");
            vSmallSize.addClass("btn-success");
            vLargeSize.removeClass("btn-warning");
            vLargeSize.addClass("btn-success");
        }
        if (paramButton === "Large") {
            vLargeSize.addClass("btn-warning");
            vSmallSize.removeClass("btn-warning");
            vSmallSize.addClass("btn-success");
            vMediumSize.removeClass("btn-warning");
            vMediumSize.addClass("btn-success");
        }
        if (paramButton === "Ocean Mania") {
            vOcean.addClass("btn-warning");
            vHawaiian.removeClass("btn-warning");
            vHawaiian.addClass("btn-success");
            vBacon.removeClass("btn-warning");
            vBacon.addClass("btn-success");
        }
        if (paramButton === "Hawaiian") {
            vHawaiian.addClass("btn-warning");
            vOcean.removeClass("btn-warning");
            vOcean.addClass("btn-success");
            vBacon.removeClass("btn-warning");
            vBacon.addClass("btn-success");
        }
        if (paramButton === "Cheesy Chicken Bacon") {
            vBacon.addClass("btn-warning");
            vOcean.removeClass("btn-warning");
            vOcean.addClass("btn-success");
            vHawaiian.removeClass("btn-warning");
            vHawaiian.addClass("btn-success");
        }
    }

    function getComboSelected(
        paramMenuName,
        paramDuongKinhCM,
        paramSuonNuong,
        paramSaladGr,
        paramDrink,
        paramPriceVND
    ) {
        var vSelectedMenu = {
            menuName: paramMenuName, // S, M, 
            duongKinhCM: paramDuongKinhCM,
            suonNuong: paramSuonNuong,
            saladGr: paramSaladGr,
            drink: paramDrink,
            priceVND: paramPriceVND,
        };
        return vSelectedMenu;
    }

    function onPageLoading() {
        getApiNgayThang();
        callApiGetDrinkData();
        loadDrinkToSelect(gDrinkList);
    }

    function callApiGetDrinkData() {
        "use strict";
        $.ajax({
            url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
            async: false,
            type: "GET",
            dataType: 'json',
            success: function(responseObject) {

                console.log(responseObject);
                gDrinkList = responseObject;
            },
            error: function(error) {
                console.assert(error.responseText);
            }
        })
    }

    function loadDrinkToSelect(paramDataObject) {
        "use strict";
        $.each(paramDataObject, function(i, item) {
            $("#inp-drink").append($('<option>', {
                text: item.tenNuocUong,
                value: item.maNuocUong
            }))
        })
    }

    function getCustomerInfo() {
        "use strict";
        var vInpHoVaTenValue = $("#inp-fullname").val().trim();
        var vInpEmailValue = $("#inp-email").val().trim();
        var vInpDienThoaiValue = $("#inp-dien-thoai").val().trim();
        var vInpDiaChiValue = $("#inp-dia-chi").val().trim();
        var vInpLoiNhanValue = $("#inp-message").val().trim();
        var vInpVoucherValue = $("#inp-voucher").val().trim();
        var vInpDrinkValue = $("#inp-drink option:selected").val();

        var vCustomerInfo = {
            menuCombo: gSelectedMenuStructure,
            loaiPizza: gSelectedPizzaType,
            nuocUong: vInpDrinkValue,
            ten: vInpHoVaTenValue,
            email: vInpEmailValue,
            dienThoai: vInpDienThoaiValue,
            diaChi: vInpDiaChiValue,
            loiNhan: vInpLoiNhanValue,
            voucher: vInpVoucherValue,

            priceAnnualVND: function() {
                // goi ham tinh phan tram

                callApiToGetVoucher(vInpVoucherValue);
                console.log(gVoucherObj);
                if (gVoucherObj != null) {
                    gPercent = gVoucherObj.phanTramGiamGia;
                } else if (gVoucherObj === null) {
                    gPercent = 0;
                }
                var vTotal = this.menuCombo.priceVND * (1 - gPercent / 100);
                return vTotal;
            }
        }
        return vCustomerInfo;
    }

    function validateData(paramCustomerInfo) {
        "use strict";
        if (paramCustomerInfo.menuCombo.menuName === "") {
            alert("Bạn hãy chọn Menu pizza!")
            return false;
        }
        if (paramCustomerInfo.loaiPizza === "") {
            alert("Bạn hãy chọn Loại pizza!")
            return false;
        }
        if (paramCustomerInfo.nuocUong === "") {
            alert("Bạn hãy chọn đồ uống!")
            return false;
        }

        if (paramCustomerInfo.ten === "") {
            alert("Bạn hãy nhập Họ và tên!")
            return false;
        }
        if (paramCustomerInfo.email == "") {
            alert("Bạn hãy nhập email");
            return false;
        }
        var vCheckEmail = validateEmail(paramCustomerInfo.email);
        if (vCheckEmail === false) {
            return false;
        }

        if (paramCustomerInfo.dienThoai === "") {
            alert("Bạn hãy nhập Số điện thoại!")
            return false;
        }
        if (paramCustomerInfo.diaChi === "") {
            alert("Bạn hãy nhập địa chỉ!")
            return false;
        }

        return true;
    }

    function validateEmail(paramEmail) {
        "use strict";
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var vCheckEmail = true;
        if (!mailformat.test(paramEmail)) {
            alert("Email chưa hợp lệ");
            vCheckEmail = false;
        }
        return vCheckEmail;
    }

    function callApiToGetVoucher(paramVoucherId) {
        "use strict";
        const vBASE_URL = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
        $.ajax({
            url: vBASE_URL + paramVoucherId,
            async: false,
            type: "GET",
            dataType: 'json',
            success: function(responseObject) {
                console.log(responseObject);
                gVoucherObj = responseObject;
                $("#inp-modal-fullname").val(gCustomerInfo.ten);
                $("#inp-modal-dien-thoai").val(gCustomerInfo.dienThoai);
                $("#inp-modal-dia-chi").val(gCustomerInfo.diaChi);
                $("#inp-modal-loi-nhan").val(gCustomerInfo.loiNhan);
                $("#inp-modal-voucher").val(gCustomerInfo.voucher);
                $("#inp-detail-info").val("Xác nhận: " + gCustomerInfo.ten + ", " + gCustomerInfo.dienThoai + ", " + gCustomerInfo.diaChi + "\n" +
                    "Menu: " + gCustomerInfo.menuCombo.menuName + ", sườn nướng " + gCustomerInfo.menuCombo.suonNuong + ", nước " + gCustomerInfo.menuCombo.drink + ", ..." + "\n" +
                    "Loại pizza: " + gCustomerInfo.loaiPizza + ", Giá " + gCustomerInfo.menuCombo.priceVND + " VNĐ, Mã giảm giá: " + gCustomerInfo.voucher + "\n" +
                    "Phải thanh toán: " + gCustomerInfo.priceAnnualVND() + " VNĐ (giảm giá) " + gPercent + "%");
            },
            error: function(error) {
                console.assert(error.responseText);
                $("#inp-modal-fullname").val(gCustomerInfo.ten);
                $("#inp-modal-dien-thoai").val(gCustomerInfo.dienThoai);
                $("#inp-modal-dia-chi").val(gCustomerInfo.diaChi);
                $("#inp-modal-loi-nhan").val(gCustomerInfo.loiNhan);
                $("#inp-modal-voucher").val(gCustomerInfo.voucher);
                $("#inp-detail-info").val("Xác nhận: " + gCustomerInfo.ten + ", " + gCustomerInfo.dienThoai + ", " + gCustomerInfo.diaChi + "\n" +
                    "Menu: " + gCustomerInfo.menuCombo.menuName + ", sườn nướng " + gCustomerInfo.menuCombo.suonNuong + ", nước " + gCustomerInfo.menuCombo.drink + ", ..." + "\n" +
                    "Loại pizza: " + gCustomerInfo.loaiPizza + ", Giá " + gCustomerInfo.menuCombo.priceVND + " VNĐ, Mã giảm giá: " + gCustomerInfo.voucher + "\n" +
                    "Phải thanh toán: " + gCustomerInfo.menuCombo.priceVND + " VNĐ (không có mã giảm giá)");
            }
        })
    }

})